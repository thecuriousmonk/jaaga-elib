var app = angular.module('jaagaApp', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider){
  $stateProvider
  	.state("app",{
  		url: "",
  		templateUrl : "js/templates/main.html",
  		controller : "mainCtrl"
  	})
   	$urlRouterProvider.otherwise("");
}); 	